import discord
import itertools

bot = discord.Bot()

northgard_group = bot.create_group("northgard", "Northgard Commands")
# warband_group = northgard_group.create_subgroup("warband", "Warband Subcommands", guild_ids=[432305063730610176])

unittypes = ["Warrior", "Shield Bearer", "Axe Thrower", "Skirmisher", "Dragonkin", "Tracker", "Shaman"]

@northgard_group.command(description="Calculate total warband cost")
@discord.option("count", description="Number of units", min_value=0)
@discord.option("type", description="Type of unit", required=True, choices=unittypes)
@discord.option("has_war_effort", description="Legions 500 Military Experience", required=False)
async def warband_cost(ctx: discord.ApplicationContext, count: int, type: str, has_war_effort: bool):
    match type:
        case "Warrior" | "Shield Bearer" | "Axe Thrower":
            base_cost, increment = 30, 6
        case "Skirmisher":
            base_cost, increment = 20, 5
        case "Dragonkin":
            base_cost, increment = 40, 10
        case "Tracker":
            base_cost, increment = 25, 5
        case "Shaman":
            base_cost, increment = 30, 5
        case _:
            raise Exception("Case not handled")

    if has_war_effort:
        base_cost = int(base_cost * 0.75)
    cost = sum(itertools.islice((x + increment for x in itertools.count(base_cost)), count)) 

    type_str = type if count == 1 else f"{type}s"
    war_effort_str = "with war effort " if has_war_effort else ""
    await ctx.respond(f"Total warband cost {war_effort_str}for {count} {type_str}: {cost}")

if __name__ == '__main__':
    with open("discord.token", "r") as f:
        bot.run(f.readline())








